let sidenav = Vue.component("side-nav", {
  data: function() {
    return {
      active: false
    };
  },
  // This is just for browser support. Laravel mix has webpack built in, so...
  template:
    '<div class="w-100">' +
    '<span class="fas fa-bars d-block text-right d-md-none h4"  @click="active = !active"></span>' +
    '<div class="side-nav-container" :class="{\'active\': active}">' +
    '<div class= "side-nav--backdrop d-md-none" @click="active = !active" ></div >' +
    
    "<slot></slot>" +
    
    "</div >" +
    "</div>"
});

var vm = new Vue({
  el: "#app",
  data: {
    navbar2: false
  },
  components: {
    sidenav,
    carousel: VueCarousel.Carousel,
    slide: VueCarousel.Slide
  },
  methods: {
    adjustNavbar() {
      document.documentElement.scrollTop > 500 ? this.navbar2 = true : this.navbar2 = false
    }
  },
  beforeMount() {
    window.addEventListener('scroll', this.adjustNavbar);
  },
});